set nu
set wrap
set linebreak
set nolist
set nopaste
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set autoindent
set smartindent
set list
set ic

call plug#begin()
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mileszs/ack.vim'
Plug 'skielbasa/vim-material-monokai'
Plug 'Valloric/YouCompleteMe'
Plug 'fatih/vim-go'

Plug 'vim-syntastic/syntastic'
Plug 'w0rp/ale'
Plug 'scrooloose/nerdtree'

Plug 'nelsyeung/twig.vim'
Plug 'pangloss/vim-javascript'

Plug 'mattn/emmet-vim'

call plug#end()

set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0

set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_semantic_triggers =  {
            \   'css': ['    ', ': '],
            \   'cs,java,javascript,typescript,d,python,perl6,scala,vb,elixir,go' : ['.'],
            \   'html': ['<', '"', '</', ' '],
            \ }

set conceallevel=2
set concealcursor=vin
let g:clang_snippets=1
let g:clang_conceal_snippets=1
let g:clang_snippets_engine='clang_complete'
" Complete options (disable preview scratch window, longest removed to aways show menu)
set completeopt=menu,menuone

" Limit popup menu height
set pumheight=20

let g:ale_emit_conflict_warnings = 0
let g:ale_fixers = {
\    'javascript': ['eslint'],
\    'html': ['htmlhint']
\}
let g:ale_html_htmlhint_options = '--config=~/.htmlhintrc'

set hlsearch

set background=dark
colorscheme material-monokai

let g:ale_echo_cursor = 0

let g:nerdtree_tabs_open_on_console_startup = 1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let g:ctrlp_working_path_mode = 0
let g:user_emmet_leader_key='<C-Z>'

autocmd FileType html,css EmmetInstall

hi MatchParen cterm=bold ctermbg=none ctermfg=green

" =======================================
" Enables cursor line position tracking:
set cursorline
" Removes the underline causes by enabling cursorline:
highlight clear CursorLine
" Sets the line numbering to red background:
highlight CursorLineNR ctermbg=darkgray